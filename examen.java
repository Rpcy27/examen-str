import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.Random;

class ExecutionThread1 extends Thread {
    Lock l1;
    int activity;
    Integer monitor;

    public void ExecutionThread(Lock l1, int activity, Integer monitor) {
        this.l1 = l1;
        this.activity = activity;
        this.monitor = monitor;
    }

    public void run() {
        while (true) {
            System.out.println(this.getName() + " - STATE 1");
            l1.lock();
			synchronized (monitor) {
                System.out.println(this.getName() + " - STATE 2");
                int l = activity;
                for (int i = 0; i < l * 100000; i++) {
                    i++;
                    i--;
                }
            }
            l1.unlock();
            System.out.println(this.getName() + " - STATE 3");
        }
    }
}

class ExecutionThread2 extends Thread {
    Lock l1;
    int sleep, activity;
    Integer monitor;

    public void ExecutionThread(Integer monitor, Lock l1, int sleep, int activity) {
        this.l1 = l1;
        this.sleep = sleep;
        this.activity = activity;
        this.monitor = monitor;
    }

    public void run() {
        while(true) {
            System.out.println(this.getName() + " - STATE 1");
            l1.lock();
            synchronized (monitor) {
                System.out.println(this.getName() + " - STATE 2");
                int l = activity;
                for (int i = 0; i < l * 100000; i++) {
                    i++;
                    i--;
                }
            }
            l1.unlock();
            System.out.println(this.getName() + " - STATE 3");
            try {
                Thread.sleep(Math.round(Math.random() * (sleep - sleep)+ sleep) * 500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(this.getName() + " - STATE 4");
        }
    }
}

class Main {
    public static void main(String[] args) {
        Lock b1=new ReentrantLock();
        Integer monitor;
        new ExecutionThread1().start();
        new ExecutionThread2().start();
    }
}
